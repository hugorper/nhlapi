# Made With the NHL API Documentation

This document exists to collect things that people make using this documentation.  There is zero affiliation with those projects other than I like to show appreciation back to those who appricate the work here and find it useful.

---

## nhl-api-explorer

Description: nuxtjs client with nodejs server (to bypass CORS headers).
Live: https://nhl-api-explorer.herokuapp.com/
Code: https://gitlab.com/sebastienblanchet/nhl-api-explorer
Author: [Sebastien Blanchet](https://gitlab.com/sebastienblanchet)

## hockey-info.online

Description: web app showing hockey information in a mobile friendly format
Live: https://www.hockey-info.online
Code: https://gitlab.com/dword4/hockey-info
Author: [Me](https://gitlab.com/dword4)
